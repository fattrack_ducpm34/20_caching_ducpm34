﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisDemo.Attributes;
using RedisDemo.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedisDemo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IResponseCacheService _responseCacheService;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IResponseCacheService responseCacheService)
        {
            _logger = logger;
            _responseCacheService = responseCacheService;
        }

        [HttpGet("getall")]
        [Cache(1000)]
        public async Task<IActionResult> GetAsync(string keyword = null, int pageIndex = 1, int pageSize = 20)
        {
            var rng = new Random();

            var result = new List<WeatherForecast>() {
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                },
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                },
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                },
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                },
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                },
                new WeatherForecast() {
                    Date = DateTime.Now.AddDays(rng.Next(1,5)),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                }
            };
            return Ok(result);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create()
        {
            await _responseCacheService.RemoveCacheResponseAsync("/WeatherForecast/getall");
            return Ok();
        }
    }
}
