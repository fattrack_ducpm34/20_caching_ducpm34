﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RedisDemo.Attributes;
using RedisDemo.Models;
using RedisDemo.Service;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;

namespace RedisDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly MagicBooksContext _context;
        private readonly IResponseCacheService _responseCacheService;

        public BooksController(MagicBooksContext context, IResponseCacheService responseCacheService)
        {
            _context = context;
            _responseCacheService = responseCacheService;
        }

        // GET: api/Books
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetBooks()
        {
            return await _context.Books.ToListAsync();
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        [Cache(1000)]
        public async Task<ActionResult<Book>> GetBook(int id)
        {
            var book = await _context.Books.FindAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        // GET: api/Books/author/5
        // Author is not set as Index. We will see a clearer difference between using Caching and not using Caching with an element.
        [HttpGet("author/{author}")]
        [Cache(1000)]
        public async Task<ActionResult<Book>> GetBookByAuthor(string author)
        {
            var book = await _context.Books.Where(x => x.Author == author).FirstOrDefaultAsync();

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }


        // GET: api/Books/title/mr
        // Title is not set as Index. We will see a clearer difference between using Caching and not using Caching with list.
        [HttpGet("title/{title}")]
        [Cache(1000)]
        public async Task<ActionResult<IEnumerable<Book>>> GetBookByTitle(string title)
        {
            var books = await _context.Books.Where(x => x.Title == title).Take(100000).ToListAsync();

            if (books == null || books.Count == 0)
            {
                return NotFound();
            }

            return books;
        }


        // PUT: api/Books/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, Book book)
        {
            await _responseCacheService.RemoveCacheResponseAsync($"/api/Books/title/{id}");

            if (id != book.BookId)
            {
                return BadRequest();
            }

            _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.BookId == id);
        }
    }
}