﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RedisDemo.Configuration;
using RedisDemo.Service;
using StackExchange.Redis;

namespace RedisDemo.Installer
{
    public class CacheInstaller : IInstaller
    {
        public void InstallerServicesInAssembly(IServiceCollection service, IConfiguration configuration)
        {
            var redisConfiguration = new RedisConfiguration();
            configuration.GetSection("RedisConfiguration").Bind(redisConfiguration);

            service.AddSingleton(redisConfiguration);

            if (!redisConfiguration.Enabled) return;

            service.AddSingleton<IConnectionMultiplexer>(_ => ConnectionMultiplexer.Connect(redisConfiguration.ConnectionString));
            service.AddStackExchangeRedisCache(option => option.Configuration = redisConfiguration.ConnectionString);
            service.AddSingleton<IResponseCacheService, ResponseCacheService>();
        }
    }
}
